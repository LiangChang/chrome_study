const webpush = require('web-push');

// VAPID keys should only be generated only once.
//const vapidKeys = webpush.generateVAPIDKeys();
const vapidKeys = {
        publicKey: "BH5XERTySFq9HsCAwawNed9DctbidVPmdCp1JURRwlzpj2NyDw10G5n_VpxmwCnEhaX1DUCKu3a_qXw8SMEuur0",
        privateKey: "vYwTzZwvbV4qsZuXpt_DxvxOVCMeCfN7UOvmyTvtQAA"
    }
    // webpush.setGCMAPIKey('vYwTzZwvbV4qsZuXpt_DxvxOVCMeCfN7UOvmyTvtQAA');
webpush.setVapidDetails(
    'mailto:example@yourdomain.org',
    vapidKeys.publicKey,
    vapidKeys.privateKey
);




// This is the same output of calling JSON.stringify on a PushSubscription
const pushSubscription = {
    endpoint: "https://updates.push.services.mozilla.com/wpush/v2/gAAAAABZrkGLg0BqSWDb6mqW4YjpX-W5RQBN8Ydd2xoGpeVXG1yHO1-rMLyCwphDWGEv71Dfj6x9R26vJKHtqo3eW7Mw4_tmkwxogbcEAgHo-NeOkLFejwb6IsPbWDXt2RgNZk3dvpQnMVdx7lt6-FfFM-ppRF7tWAiGa_KS5dwENcPjWW4hxSg",
    keys: {
        auth: "fu47Sj7IqRFoQd8Y-8jEZA",
        p256dh: "BFJPycZpOrFVshoPCqIc9_OjVpZP5WK8LRLJnEu-xQfYV0AoIBhTMOTEDuOtN4n3n0uGZ0cnXZRCkpBmmbV8-RY"
    }
};

const payload = 'This is a test.';

// const options = {
//     gcmAPIKey: '< GCM API Key >',
//     vapidDetails: {
//         subject: '< \'mailto\' Address or URL >',
//         publicKey: '< URL Safe Base64 Encoded Public Key >',
//         privateKey: '< URL Safe Base64 Encoded Private Key >'
//     },
//     TTL: < Number > ,
//     headers: {
//         '< header name >': '< header value >'
//     }
// }

webpush.sendNotification(
        pushSubscription,
        payload
    ).then(success => {
        console.log(success);
    })
    .catch(success => {
        debugger;
        console.log(success);
    });